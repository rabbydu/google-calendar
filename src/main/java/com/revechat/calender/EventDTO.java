package com.revechat.calender;

import java.util.ArrayList;

public class EventDTO {

	private String summery;
	private String location;
	private String description;
	private long startTime;
	private long endTime;
	private ArrayList<AttendeeDTO> attendees;
	public String getSummery() {
		return summery;
	}
	public void setSummery(String summery) {
		this.summery = summery;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	public ArrayList<AttendeeDTO> getAttendees() {
		return attendees;
	}
	public void setAttendees(ArrayList<AttendeeDTO> attendees) {
		this.attendees = attendees;
	}
	
	
}
