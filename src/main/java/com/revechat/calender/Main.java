package com.revechat.calender;

import java.util.ArrayList;
import java.util.Date;

public class Main {

	public static void main(String args[]) {
		testEventList("12159", "bb8e9e93-95fb-4886-bba2-e1b7bd9ce819");
		//testAddEvent("12159", "bb8e9e93-95fb-4886-bba2-e1b7bd9ce819");
	}

	private static void testEventList(String accountId, String botId) {
		CalenderResponse response = CalenderService.getInstance().getEvents(accountId, botId);
		if (response != null && response.isSuccess()) {
			System.out.println("EventList get Sussessfully");
		} else {
			System.out.println("EventList get failed");
		}
	}

	private static void testAddEvent(String accountId, String botId) {

		AttendeeDTO att1 = new AttendeeDTO();
		att1.setEmail("abc@gmail.com");
		att1.setName("Att 1");

		AttendeeDTO att2 = new AttendeeDTO();
		att2.setEmail("def@gmail.com");
		att2.setName("Att 2");

		ArrayList<AttendeeDTO> listAtt = new ArrayList<AttendeeDTO>();
		listAtt.add(att1);
		listAtt.add(att2);

		EventDTO eventDTO = new EventDTO();
		eventDTO.setSummery("New appointment");
		eventDTO.setLocation("Dhaka, Bangladesh");
		eventDTO.setDescription("Sample entry into google canlender");
		eventDTO.setStartTime(new Date().getTime());
		eventDTO.setEndTime(new Date().getTime() + (30 * 60 * 1000));
		eventDTO.setAttendees(listAtt);

		CalenderResponse response = CalenderService.getInstance().addEvent(accountId, botId, eventDTO);

		if (response != null && response.isSuccess()) {
			System.out.println("Event added Sussessfully");
		} else {
			System.out.println("Event added failed");
		}
	}
}
