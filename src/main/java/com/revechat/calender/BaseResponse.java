package com.revechat.calender;

public class BaseResponse {

	public static final int ERROR_CODE_ACCESS_TOKEN_EXPIRES = 1;
	public static final int ERROR_CODE_OTHERS = 2;

	private boolean success;
	private String message;
	private int errorCode;

	public BaseResponse() {
		this.success = true;
		this.errorCode = 0;
		this.message = "";
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
