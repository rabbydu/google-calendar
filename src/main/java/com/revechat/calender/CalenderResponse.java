package com.revechat.calender;

import java.util.ArrayList;

public class CalenderResponse extends BaseResponse {

	private ArrayList<EventResponseDTO> eventList;

	public ArrayList<EventResponseDTO> getEventList() {
		return eventList;
	}

	public void setEventList(ArrayList<EventResponseDTO> eventList) {
		this.eventList = eventList;
	}

}
