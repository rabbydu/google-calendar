package com.revechat.calender;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

import test.AccessToken;

public class CalenderService {

	private static final CalenderService instance = new CalenderService();

	private CalenderService() {
	};

	public static CalenderService getInstance() {
		return instance;
	}

	public CalenderResponse addEvent(String accountId, String botId, EventDTO eventDTO) {
		
		CalenderResponse response = null;
		
		CalendarInfoDTO calendarInfoDTO = this.getGoogleLoginInfo(accountId, botId);

		if (calendarInfoDTO != null) {
			response = this.addNewEvent(calendarInfoDTO.getAccessToken(), eventDTO);

			if (response.isSuccess()) {
				System.out.println("Event added Sussessfully");
			} else {
				if (response.getErrorCode() == BaseResponse.ERROR_CODE_ACCESS_TOKEN_EXPIRES) {
					String newAccessToken = this.getNewAccessToken(calendarInfoDTO.getRefreshToken());
					calendarInfoDTO.setAccessToken(newAccessToken);

					this.updateAccessToken(accountId, botId, calendarInfoDTO);

					response = this.addNewEvent(newAccessToken, eventDTO);
					if (response.isSuccess()) {
						System.out.println("Event added Sussessfully");
					}
				} else {
					System.out.println("Failed to add event");
				}
			}
		} else {
			response = null;
		}
		return response;
	}

	public CalenderResponse getEvents(String accountId, String botId) {
		
		CalenderResponse response = null;

		CalendarInfoDTO calendarInfoDTO = this.getGoogleLoginInfo(accountId, botId);

		if (calendarInfoDTO != null) {
			response = this.getEventList(calendarInfoDTO.getAccessToken());
			if (response.isSuccess()) {
				System.out.println("EventList get Sussessfully");
			} else {
				if (response.getErrorCode() == BaseResponse.ERROR_CODE_ACCESS_TOKEN_EXPIRES) {

					String newAccessToken = this.getNewAccessToken(calendarInfoDTO.getRefreshToken());
					calendarInfoDTO.setAccessToken(newAccessToken);

					this.updateAccessToken(accountId, botId, calendarInfoDTO);

					response = this.getEventList(newAccessToken);
					if (response.isSuccess()) {
						System.out.println("EventList get Sussessfully");
					}
				} else {
					System.out.println("Failed to get event list");
				}
			}
		} else {
			response = null;
		}
		return response;
	}

	private CalendarInfoDTO getGoogleLoginInfo(String accountId, String botId) {
		return CalendarDAO.getInstance().getGoogleLoginInfo(accountId, botId);
	}

	private boolean updateAccessToken(String accountId, String botId, CalendarInfoDTO dto) {
		return CalendarDAO.getInstance().updateAccessToken(accountId, botId, dto);
	}

	private CalenderResponse addNewEvent(String accessToken, EventDTO dto) {

		CalenderResponse response = new CalenderResponse();

		try {

			DateTime startDateTime = new DateTime(dto.getStartTime());
			EventDateTime start = new EventDateTime().setDateTime(startDateTime).setTimeZone("America/Los_Angeles");

			DateTime endDateTime = new DateTime(dto.getEndTime());
			EventDateTime end = new EventDateTime().setDateTime(endDateTime).setTimeZone("America/Los_Angeles");

			ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
			for (AttendeeDTO attendeeDTO : dto.getAttendees()) {
				attendees.add(new EventAttendee().setEmail(attendeeDTO.getEmail()).setDisplayName(attendeeDTO.getName()));
			}

			Event event = new Event();
			event.setSummary(dto.getSummery());
			event.setLocation(dto.getLocation());
			event.setDescription(dto.getDescription());
			event.setStart(start);
			event.setEnd(end);
			event.setAttendees(attendees);

			NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Credential credential = new Credential(BearerToken.authorizationHeaderAccessMethod()).setAccessToken(accessToken);
			Calendar service = new Calendar.Builder(HTTP_TRANSPORT, Constants.JSON_FACTORY, credential)
					.setApplicationName(Constants.APPLICATION_NAME).build();

			event = service.events().insert(Constants.CALENDER_ID, event).execute();

			System.out.printf("Event created: %s\n", event.getHtmlLink());

		} catch (GoogleJsonResponseException e) {
			System.err.println("Error: " + e);
			response.setSuccess(false);
			response.setErrorCode(BaseResponse.ERROR_CODE_ACCESS_TOKEN_EXPIRES);
			response.setMessage("Access Token expires");
		} catch (Exception e) {
			System.err.println("Error: " + e);
			response.setSuccess(false);
			response.setErrorCode(BaseResponse.ERROR_CODE_OTHERS);
			response.setMessage("Exception occured: " + e.getMessage());
		}
		return response;
	}

	private CalenderResponse getEventList(String accessToken) {

		CalenderResponse response = new CalenderResponse();

		ArrayList<EventResponseDTO> list = new ArrayList<EventResponseDTO>();

		try {

			Credential credential = new Credential(BearerToken.authorizationHeaderAccessMethod()).setAccessToken(accessToken);
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Calendar service = new Calendar.Builder(HTTP_TRANSPORT, Constants.JSON_FACTORY, credential)
					.setApplicationName(Constants.APPLICATION_NAME).build();

			DateTime now = new DateTime(System.currentTimeMillis());

			Events events = service.events().list("primary").setTimeMin(now).execute();
			List<Event> items = events.getItems();

			if (items.isEmpty()) {
				System.out.println("No upcoming events found.");
			} else {
				System.out.println("Upcoming events");
				for (Event event : items) {
					EventResponseDTO dto = new EventResponseDTO();
					dto.setSummery(event.getSummary());
					dto.setDate(event.getStart().getDateTime().getValue());

					list.add(dto);

					DateTime start = event.getStart().getDateTime();
					if (start == null) {
						start = event.getStart().getDate();
					}
					System.out.println(dto.getSummery() + " " + dto.getDate());
				}
			}

			response.setEventList(list);

		} catch (GoogleJsonResponseException e) {
			System.err.println("Error: " + e);
			response.setSuccess(false);
			response.setErrorCode(BaseResponse.ERROR_CODE_ACCESS_TOKEN_EXPIRES);
			response.setMessage("Access Token expires");
		} catch (Exception e) {
			System.err.println("Error: " + e);
			response.setSuccess(false);
			response.setErrorCode(BaseResponse.ERROR_CODE_OTHERS);
			response.setMessage("Execption occured: " + e.getMessage());
		}

		return response;

	}

	public String getNewAccessToken(String refreshToken) {

		String accessToken = null;

		System.out.println("getNewAccessToken().... running ....");
		try {

			GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(Constants.JSON_FACTORY,
					new InputStreamReader(AccessToken.class.getResourceAsStream("/client_secrets.json")));

			String data = "client_id=" + clientSecrets.getDetails().getClientId() + "&client_secret="
					+ clientSecrets.getDetails().getClientSecret() + "&refresh_token=" + refreshToken
					+ "&grant_type=refresh_token";

			URL obj = new URL(Constants.TOKEN_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");

			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Access-Control-Allow-Origin", "*");
			con.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

			con.setDoOutput(true);
			con.setDoInput(true);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			// System.out.println("getNewAccessToken(): response: " + response.toString());

			JSONObject jsonObj = new JSONObject(response.toString());

			accessToken = jsonObj.getString("access_token");

			System.out.println("getNewAccessToken(): access_token: " + accessToken);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return accessToken;
	}
}
