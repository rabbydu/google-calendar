package com.revechat.calender;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONObject;

import databasemanager.DatabaseManager;

public class CalendarDAO {
	
	private static final CalendarDAO instance = new CalendarDAO();
	
	private CalendarDAO() {};
	
	public static CalendarDAO getInstance() {
		return instance;
	}

	public CalendarInfoDTO getGoogleLoginInfo(String accountId, String botId) {
		CalendarInfoDTO response = null;

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "SELECT google_login FROM vbbotinfo WHERE vbaccount = ? AND botId = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, accountId);
			ps.setString(2, botId);

			rs = ps.executeQuery();

			if (rs.next()) {
				String msg = rs.getString("google_login");
				if (msg != null && !msg.equals("")) {
					response = new CalendarInfoDTO();

					JSONObject jsonObject = new JSONObject(msg);
					response.setAccessToken(jsonObject.getString("accessToken"));
					response.setRefreshToken(jsonObject.getString("refreshToken"));
					response.setEmail(jsonObject.getString("email"));
					response.setFullName(jsonObject.getString("fullName"));
					response.setBotId(jsonObject.getString("botId"));
					response.setAuthCode(jsonObject.getString("authCode"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}
		}
		return response;

	}

	public boolean updateAccessToken(String accountId, String botId, CalendarInfoDTO dto) {

		boolean result = true;

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			JSONObject msg = new JSONObject(dto);
			
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE vbbotinfo SET google_login = ? WHERE vbaccount = ? AND botId = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, msg.toString());
			ps.setString(2, accountId);
			ps.setString(2, botId);

		} catch (Exception e) {
			result = false;
		} finally {
			try {
				ps.close();
				rs.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}
		}

		return result;
	}
}
