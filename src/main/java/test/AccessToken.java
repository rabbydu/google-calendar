package test;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import com.google.api.services.calendar.model.Events;

public class AccessToken {
	private static final String APPLICATION_NAME = "Calendar";

	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".store/plus_sample");

	private static FileDataStoreFactory dataStoreFactory;

	private static HttpTransport httpTransport;

	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static final String CLIENT_SECRET_FILE = "/client_secrets.json";

	private static final String REDIRECT_URI = "https://staging-dev.revechat.com:5443";

	public static void main(String args[]) {

		// Working code

		try {

			// load client secrets
			GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
					new InputStreamReader(AccessToken.class.getResourceAsStream("/client_secrets.json")));

			if (clientSecrets.getDetails().getClientId().startsWith("Enter")
					|| clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {

				System.out.println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=plus "
						+ "into plus-cmdline-sample/src/main/resources/client_secrets.json");

				System.exit(1);
			}

			System.out.println("Client ID: " + clientSecrets.getDetails().getClientId());
			System.out.println("Secret ID: " + clientSecrets.getDetails().getClientSecret());

			String authCode = "4/vgGpqhq_Cgi6sr5xSQb17e5YQoGndv-gtrH4HTK-RmSVXduExJ5w0aFJC9dF4hexkvWxkUYiL27wt1u9l_Yva5g";

//			GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
//					new NetHttpTransport(),
//					JacksonFactory.getDefaultInstance(), 
//					"https://oauth2.googleapis.com/token",
//					clientSecrets.getDetails().getClientId(), 
//					clientSecrets.getDetails().getClientSecret(), 
//					authCode,
//					REDIRECT_URI).execute(); // Specify the same redirect URI that you use with your web
//									// app. If you don't have a web version of your app, you can
//									// specify an empty string.
//							
//
//			String accessToken = tokenResponse.getAccessToken();
//			String refreshToken = tokenResponse.getRefreshToken();
//
//			System.out.println("Access token: " + accessToken);
//			System.out.println("Refresh token: " + refreshToken);

			// GoogleCredential credential = new
			// GoogleCredential().setAccessToken(accessToken);

			String accessToken = "ya29.ImC6BzphAlHDRMdJWRDgEQM-fiXorf6MXU-HdaY1cmjeU-K1LrR3ZImOfR7hD8dwNKoW_iyRv9gMTMmpw303IEv-iOr1HVB9oX9A-O-F4pxgjydGdMtLvyInAZY6kM2cRBc";

			Credential credential = new Credential(BearerToken.authorizationHeaderAccessMethod()).setAccessToken(accessToken);

			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
					.build();
			
			System.out.println("calender service: " + service.events().list("primary"));

			// List the next 10 events from the primary calendar.
			DateTime now = new DateTime(System.currentTimeMillis());
			Events events = service.events().list("primary").setMaxResults(10).setTimeMin(now).setOrderBy("startTime")
					.setSingleEvents(true).execute();
			List<Event> items = events.getItems();
			if (items.isEmpty()) {
				System.out.println("No upcoming events found.");
			} else {
				System.out.println("Upcoming events");
				for (Event event : items) {
					DateTime start = event.getStart().getDateTime();
					if (start == null) {
						start = event.getStart().getDate();
					}
					System.out.printf("%s (%s)\n", event.getSummary(), start);
				}
			}

			// Create event
			Event event = new Event()
					.setSummary("Google I/O 2015")
					.setLocation("800 Howard St., San Francisco, CA 94103")
					.setDescription("A chance to hear more about Google's developer products.");

			DateTime startDateTime = new DateTime("2020-01-22T12:00:00-07:00");
			EventDateTime start = new EventDateTime().setDateTime(startDateTime).setTimeZone("America/Los_Angeles");
			event.setStart(start);

			DateTime endDateTime = new DateTime("2020-01-22T13:00:00-07:00");
			EventDateTime end = new EventDateTime().setDateTime(endDateTime).setTimeZone("America/Los_Angeles");
			event.setEnd(end);

			String[] recurrence = new String[] { "RRULE:FREQ=DAILY;COUNT=2" };
			event.setRecurrence(Arrays.asList(recurrence));

			EventAttendee[] attendees = new EventAttendee[] { 
					new EventAttendee().setEmail("lpage@example.com"),
					new EventAttendee().setEmail("sbrin@example.com"), };
			event.setAttendees(Arrays.asList(attendees));

			EventReminder[] reminderOverrides = new EventReminder[] { new EventReminder().setMethod("email").setMinutes(24 * 60),
					new EventReminder().setMethod("popup").setMinutes(10), };

			Event.Reminders reminders = new Event.Reminders().setUseDefault(false).setOverrides(Arrays.asList(reminderOverrides));
			event.setReminders(reminders);

			String calendarId = "primary";
			event = service.events().insert(calendarId, event).execute();
			System.out.printf("Event created: %s\n", event.getHtmlLink());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
}
